/*
 * HOT Tasking Manager Tasks Grid styling.
 */

meta {
    title: "TM Tasks Grid";
    author: "peculiar theater";
    version: "2022.11a";
    description: "Style the downloadable Tasks Grid from the Tasking Manager to present task information on the JOSM map view.";
    link: "https://gitlab.com/peculiar-theater/mapcss/-/tree/main/tm-taskgrid";
}

/* Style the Tasks Grid geojson in a similar way to the TM itself, */
way[taskStatus=VALIDATED]   { fill-color: green;   fill-opacity: 10%; z-index: -1; }
way[taskStatus=BADIMAGERY]  { fill-color: silver;  fill-opacity: 10%; }
way[taskStatus=MAPPED]      { fill-color: skyblue; fill-opacity: 10%; }
way[taskStatus=READY]       { fill-color: white;   fill-opacity: 10%; }
way[taskStatus=INVALIDATED] { fill-color: yellow;  fill-opacity: 10%; }
way[taskStatus=LOCKED_FOR_MAPPING] { fill-color: orange; fill-opacity: 10%; }
way[taskStatus=LOCKED_FOR_VALIDATION] { fill-color: orange; fill-opacity: 10%; }

/* Annotate on the task IDs when the tasks are large enough on screen. */
area|z11-[taskStatus && taskId && areasize() > 4000000] { text: "taskId"; }
area|z13-[taskStatus && taskId && areasize() >  100000] { text: "taskId"; }
area|z14-[taskStatus && taskId && areasize() >   50000] { text: "taskId"; }
area|z16-[taskStatus && taskId                        ] { text: "taskId"; }
area|z15-[taskStatus][taskId]     { font-size: 12; text-halo-radius: 2; }
area|z16-[taskStatus][taskId]     { font-size: 18; text-halo-radius: 3; }

area[taskStatus][taskId] > node { symbol-size: 1; }
