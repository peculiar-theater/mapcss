# Tasking Manager Tasks Grid MapCSS

This [MapCSS] style colorizes a task grid GeoJSON layer in [JOSM] and annotates on task numbers for easy reference.

![A task grid showing tasks in various states, overlayed on background imagery](image.webp)

[mapcss]: https://josm.openstreetmap.de/wiki/Help/Styles/MapCSSImplementation
[josm]: https://josm.openstreetmap.de/
