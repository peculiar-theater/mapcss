# OSM metadata annotation MapCSS

This [MapCSS] style colorizes objects in [JOSM] based on who mapped them most recently.  For example, some city blocks
mapped and remapped by multiple mappers with some changes and additions in the top-left corner (shown unhiglighted):

![](image.webp)

[mapcss]: https://josm.openstreetmap.de/wiki/Help/Styles/MapCSSImplementation
[josm]: https://josm.openstreetmap.de/
